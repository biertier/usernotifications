//
//  NotificationViewController.swift
//  myContentExtension
//
//  Created by Tobias Biermeier on 7/1/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class NotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any required interface initialization here.
    }
    
    func didReceive(_ notification: UNNotification) {
        
        guard let attachement = notification.request.content.attachments.first else {
            return
        }
        
        if attachement.url.startAccessingSecurityScopedResource() {
            
            let imageData = try? Data.init(contentsOf: attachement.url)
            if let image = imageData {
                imageView.image = UIImage(data: image)
            }
            
        }
        
    }

    func didReceive(_ response: UNNotificationResponse, completionHandler completion: @escaping (UNNotificationContentExtensionResponseOption) -> Void) {
        
        if response.actionIdentifier == "fistBump" {
            completion(.dismissAndForwardAction)
        } else if response.actionIdentifier == "dismiss" {
            completion(.dismissAndForwardAction)
        }
        
    }
    
}






