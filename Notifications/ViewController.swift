//
//  ViewController.swift
//  Notifications
//
//  Created by Tobias Biermeier on 7/1/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //1. Request Permission
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            
            if granted {
                
                print("Notification access granted")
                
            } else {
                
                print(error?.localizedDescription as Any)
                
            }
            
        }
    }
    
    @IBAction func notifyButtonTapped(sender: UIButton) {
        
        scheduleNotification(inSeconds: 5) { (success) in
            
            if success {
                
                print("Successfully scheduled notification")
            } else {
                print("Error scheduling notification")
            }
            
        }
        
    }
    
    func scheduleNotification(inSeconds: TimeInterval, completion: @escaping (_ Success: Bool) -> ()) {
        
        // Add an attachment 
        
        let myImage = "rick_grimes"
        guard let imageUrl = Bundle.main.url(forResource: myImage, withExtension: ".gif") else {
            completion(false)
            return
        }
        
        var attachement: UNNotificationAttachment
        
        attachement = try! UNNotificationAttachment(identifier: "myNotification", url: imageUrl, options: .none)
        
        
        
        let notif = UNMutableNotificationContent()
        
        // Only for extension
        notif.categoryIdentifier = "myNotificationCategory"
        
        notif.title = "NewNotification"
        notif.subtitle = "There are great"
        notif.body = "This is the body of the notification"
        
        notif.attachments = [attachement]
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: inSeconds, repeats: false)
        
        let request = UNNotificationRequest(identifier: "myNotification", content: notif, trigger: notificationTrigger)
        
        UNUserNotificationCenter.current().add(request) { (error) in
            if error != nil {
             
                print((error) as Any)
                completion(false)
                
            } else {
                
                completion(true)
                
            }
        }
        
    }

}

















